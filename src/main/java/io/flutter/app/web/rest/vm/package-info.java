/**
 * View Models used by Spring MVC REST controllers.
 */
package io.flutter.app.web.rest.vm;
